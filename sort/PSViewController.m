//
//  PSViewController.m
//  sort
//
//  Created by Artem on 08.04.14.
//  Copyright (c) 2014 Artem. All rights reserved.
//

#import "PSViewController.h"

@interface PSViewController ()

@end

@implementation PSViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
 //   int   i, j, a;
   // int n = 20;
   // int x=10000000;
//	int array[] = {20,1,19,2,18,3,17,16,15,4,5,7,8,6,14,13,12,9,10,111};
    
    //сортировка пузырьком
    // внешний цикл которым мы проходимся по массиву, выполняется при прохождении вложенного цикла
    /*   for (i = 0; i < 20; i++) {
        for (j = 0; j <= n ; j++) {   //вложенный цикл  который при выполнении условия что текущий элемент больше соседнего выполняет перестановку
            if (array[j] > array[j+1]) {
                int tmp = array[j]; array[j] = array[j+1]; array[j+1] = tmp;
            }
        }
    }
    for (j=0; j<n; j++) {
         NSLog(@"%d", array[j]);
    } */
    
    //сортировка вставками
    /*    for (i = 1; i < 20; i ++) {
        for (j = i; j > 0 && array[j-1] > array[j]; j --) {
            int tmp = array[j-1];
            array[j-1] = array[j];
            array[j] = tmp;
        }
    }
    for (j = 0; j < n; j ++) {
        NSLog(@"%d", array[j]);
    } */
    
//cортировка выбором
  /*  for (i = 0; i < 20 - 1; i ++) {
        int min=i;
        for (int j = i + 1 ; j < 20; j ++) {
            if (array[j] < array[min]) {
                min = j;
            }
        }
        int temp= array[i];
        array[i]=array[min];
        array[min]=temp;
    }
    for (j = 0; j < n; j ++) {
        NSLog(@"%d", array[j]);
    }
    */
 
 
    NSMutableArray *source = [[NSMutableArray alloc] initWithObjects:@18897109, @12828837, @9461105, @6371773, @5965343, @5946800, @5582170, @5564635, @5268860, @4552402, @4335391, @4296250, @4224851, @4192887, @3439809, @3279833, @3095313, @2812896, @2783243, @2710489, @2543482, @2356285, @2226009, @2149127, @2142508, @2134411, nil];
	//NSLog(@"%@", source);
	
    
	//NSMutableArray *source = [[NSMutableArray alloc] initWithObjects:@11, @9, @8, @7, @3, @1, nil];
    
	
	for (int i = 0; i < source.count; i++) {
		NSMutableArray *result = [[NSMutableArray alloc] init];
		
		if ([self getArray:source sum:100000000 index:i resultArray:result]){
			NSLog(@"Result : %@", result);
			int sum = 0;
			for(NSNumber *num in result) {
				sum += num.intValue;
			}
			break;
		}
		
	}
}

- (BOOL)getArray:(NSMutableArray *) sourceArray sum:(int)sum index:(int)index resultArray:(NSMutableArray *) resultArray{
	
	
	if ( sum <= 0 || [sourceArray[index] intValue] > sum) {
		return NO;
	}
	
	if([sourceArray[index] intValue] <= sum){
		
	//	[resultArray addObject:sourceArray[index]];
		
		sum -= [sourceArray[index] intValue];
		
		if (sum == 0) {
			//NSLog(@"Result %@", resultArray);
			return YES;
		}
		
	}
	if (index == sourceArray.count-1) {
		return NO;
	}
	BOOL solution = NO;
	for (int i = index + 1; i < sourceArray.count; i++) {
		solution = [self getArray:sourceArray sum:sum index:i resultArray:resultArray];
		if (solution == YES) {
			[resultArray addObject:sourceArray[index]];

			return YES;
		}
	}
	return NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
   
}

@end
